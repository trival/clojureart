(ns clojureart.gol.main
  (:use quil.core 
        quil.helpers.seqs
        [quil.helpers.calc :only [mul-add]]))


(def board-scale 2)


(defn random-oct-points []
    (map vector 
       (repeatedly #(rand-int (quot (width) board-scale))) 
       (repeatedly #(rand-int (quot (height) board-scale)))))


(defn random-hex-points []
  (for [ys (repeatedly #(rand-int (quot (height) (* board-scale 2))))
        :let [x (* 2 (rand-int (quot (width) (* board-scale 2))))
              y (* 2 ys)]]
    (if (even? ys)
      [x y] [(+ x 1) y])))


(defn neighbours-oct [[x y]]
  (for [dx [-1 0 1]
        dy [-1 0 1]
        :when (not= 0 dx dy)]
    [(+ dx x) (+ dy y)]))


(defn neighbours-and-self-oct [[x y]]
  (for [dx [-1 0 1]
        dy [-1 0 1]]
    [(+ dx x) (+ dy y)]))


(defn neighbours-hex [[x y]]
  (map (fn [[dx dy]] [(+ dx x) (+ dy y)])
       [[-1 -2] [1 -2] [-2 0] [2 0] [-1 2] [1 2]]))


(defn stepper 
  "create a life-game step function with the specified rules
   for neighbours, and a live function which takes 
   the current state and the number of neighbours and tells if
   the cell is alive or not"
  [neighbours live?]
  (fn [cells] 
    (set (for [[cell n] (frequencies (reduce into [] (map neighbours cells)))
               :when (live? (contains? cells cell) n)]
           cell))))


(def step-game-of-life 
  (stepper neighbours-oct 
           (fn [alive? n] (if alive? (#{2 3} n) (#{3} n)))))


(def step-game-of-life-hex 
  (stepper neighbours-hex 
           (fn [alive? n] (if alive? (#{4 3} n) (#{2} n)))))


(def step-vichniac-vote 
  (stepper neighbours-and-self-oct #(or (= %2 4) (> %2 5))))


; ------------------------ Rendering --------------------

(def living (atom #{}))


(defn setup []
  (smooth)
  (no-stroke)
  (frame-rate 5))


(comment (defn mouse-dragged []
  (let [x (quot (mouse-x) board-scale)
        y (quot (mouse-y) board-scale)]
    (swap! living
           conj [x y]))))


(defn mouse-clicked []
  (let [rand-scale 4
        w (quot (width)  (* rand-scale board-scale))
        h (quot (height) (* rand-scale board-scale))]
    (swap! living into
         (take (* w h) (random-oct-points)))))


(defn draw [] 
  (swap! living step-game-of-life)
  (fill 255 150)
  (rect 0 0 (width) (height))
  (fill 160 190 0)
  (doseq [[x y]  @living]
    (ellipse (mul-add x board-scale (quot board-scale 2)) 
             (mul-add y board-scale (quot board-scale 2)) 
             (* board-scale 2) (* board-scale 2))))


(defsketch gol
  :title "game of life"
  :setup setup
  :draw draw
  :mouse-dragged mouse-dragged
  :mouse-clicked mouse-clicked
  :size [800 800])