(ns clojureart.maze.main 
  (:use quil.core 
        quil.helpers.seqs
        [quil.helpers.calc :only [mul-add]]))


(defn grid [w h]
  (set (concat (for [x (range w) y (range h)
                     :when (not= y (dec h))] 
                 (sorted-set [x y] [x (inc y)]))
               
               (for [y (range h) x (range w)
                     :when (not= x (dec w))] 
                 (sorted-set [x y] [(inc x) y])))))


(defn paths [walls]
  (reduce (fn [loc-map [loc1 loc2]]
            (merge-with into loc-map {loc1 [loc2] loc2 [loc1]}))
          {} (map seq walls)))


(defn gen-maze [walls]
  (let [paths (paths walls)]
    (loop [walls walls 
           unvisited (disj (set (keys paths)) (rand-nth (keys paths)))]
      (if-let [loc (rand-nth (seq unvisited))]
        (let [walks (iterate (comp rand-nth paths) loc)
              walk (take-while unvisited walks)
              doors (map set (apply merge (map hash-map walk (next walks))))]
;              doors (map set (zipmap walk (next walks)))]
          (recur (reduce disj walls doors) 
                 (reduce disj unvisited walk)))
        walls))))


; ================== rendering =======================

(def grid-size 30)


(def maze (gen-maze (grid 20 20)))


(defn setup []
  (stroke 0)
  (stroke-weight 3))


(defn draw []
  (translate 30 30)
  (rect 0 0 (* 20 30) (* 20 30))
  (doseq [[[x1 y1] [x2 y2]] (map vec maze)]
    (line (* grid-size x2) (* grid-size y2) 
          (* grid-size (+ 1 x1)) (* grid-size (+ 1 y1)))))


(defsketch great-maze
  :title "super maze generator"
  :setup setup
  :draw draw
  :size [800 800])