(ns clojureart.test.noise1
  (:use quil.core 
        quil.helpers.seqs
        [quil.helpers.calc :only [mul-add]]))

(def noise-scale 0.01)

(defn draw-point 
  [x y noise-fac]
  (fill 255 (* noise-fac 255))
  (stroke 255 (* noise-fac 255))
  (line x y (inc x) (inc y)))

(defn draw []
  (background 25 70 150)
  (let [[noisex noisey nz] ((state :noise-starts))]
    (doseq [y (range (height))
            x (range (width))]
      (let [nx (mul-add x noise-scale noisex)
            ny (mul-add y noise-scale noisey)]
        (draw-point x y (noise nx ny nz))))))

(defn noise-step [start]
  (tally (map #(mul-add % 0.1 -0.05) (map noise (steps start noise-scale)))))

(defn noise-coords []
  (let [noisex (noise-step (random 10))
        noisey (noise-step (random 10))
        noisez (steps (random 10) noise-scale)]
    (map list noisex noisey noisez)))

(defn setup []
  (frame-rate 20)
  (set-state! :noise-starts (seq->stream (noise-coords))))

(defsketch noise1
  :title "noise animation"
  :setup setup
  :draw draw
  :size [200 200])
