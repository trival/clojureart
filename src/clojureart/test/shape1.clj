(ns clojureart.test.shape1
  (:use quil.core))

(defn setup []
  (frame-rate 1)
  (background 150)
  (stroke 0 50)
  (fill 255 200))

(defn draw-point [x y noise-fac]
  (push-matrix)
  (translate (* x noise-fac 6) (* y noise-fac 6), (- y))
  (ellipse 0 0 (* noise-fac 26) (* noise-fac 26))
  (pop-matrix))

(defn draw []
  (background 150)
  (translate (/ (width) 2) (/ (height) 2) 0)
  (let [starty (random 10)
        startx (random 10)
        boundx (/ (width) 8)
        boundy (/ (height) 8)]
    (doseq [x (range (- boundx) boundx )
            y (range (- boundy) boundy )]
             (let [noisex (+ (* x 0.01) startx)
                   noisey (+ (* y 0.01) starty)]
               (draw-point x y (noise noisex noisey))))))


(defsketch example                  
  :title "first noisy shapes"  
  :setup setup                      
  :draw draw                        
  :size [1800 1000]
  :renderer :opengl) 
